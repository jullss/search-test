import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewPlacePage  } from '../new-place/new-place';
import { SearchPage } from '../search/search';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  onLoadPlace() {
    this.navCtrl.push(NewPlacePage);
  }

  showSearch() {
    this.navCtrl.push(SearchPage);
  }

}
