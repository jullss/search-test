#Install

Most of the tooling in the CLI is based on Node and is managed through npm.
Run 'npm install -g cordova ionic'

# Search

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 5.2.11.

## Development server

Run `ionic serve` for a dev server. Navigate to `http://localhost:8100/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ionic generate page Example` to generate a new page. You can also use `ionic generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

## Running unit tests

## Running end-to-end tests

## Further help
